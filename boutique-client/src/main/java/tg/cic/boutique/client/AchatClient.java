/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tg.cic.boutique.client;

import java.util.LinkedList;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import tg.cic.boutique.entites.Achat;

/**
 *
 * @author madara
 */

public class AchatClient {
    
    private final String url = "http://localhost:8080/boutique-web/resources/achat";
    private Boutique_web_HTTP_request request;

    public AchatClient() {
    }
    
    public void liste(){
        Response response = Boutique_web_HTTP_request.getRequest(url, "/liste").get();
    
        if (response.getStatus() == 200) {
            LinkedList<Achat> liste_achat = response.readEntity(new GenericType<LinkedList<Achat>>(){});
            System.out.println(liste_achat);
        } 
        else {
                System.out.println("Lister: erreur " + response.getStatus());
            }
    }
    
    public boolean trouver(int id){
        Response response = Boutique_web_HTTP_request.getRequest(url, "/trouver/" + id).get();
    
        if (response.getStatus() == 200) {
            Achat achat = response.readEntity(Achat.class);
            System.out.println(achat);
            return true;
        } 
        else if (response.getStatus() == 204) {
            System.out.println("La resource demande est introuvable"); 
            return false;
        }
        else {
                System.out.println("Trouver: erreur " + response.getStatus());
                return false;
            }
    }
    
    public void compter(){
        Response response = Boutique_web_HTTP_request.getRequest(url, "/nombre").get();
    
        if (response.getStatus() == 200) {
            int nombre_achat = response.readEntity(int.class);
            System.out.println(nombre_achat);
        } 
        else {
                System.out.println("Copmter: erreur " + response.getStatus());
            }
    }
    
    public void ajouter(Achat achat){
        Response response = Boutique_web_HTTP_request.getRequest(url, "").post(Entity.json(achat));
    
        if (response.getStatus() == 204) {
            System.out.println("Operation reussi !!");
            System.out.println("Liste des achats: "); 
            this.liste();
        } 
        else {
                System.out.println("Ajouter: erreur " + response.getStatus());
            }
    }
    
    public void supprimer(int id){
        if(trouver(id)){
            Response response = Boutique_web_HTTP_request.getRequest(url, "/" + id).delete();

            if (response.getStatus() == 204) {
                System.out.println("Operation reussi !!");
                System.out.println("Liste des achats: "); 
                this.liste();
            } 
            else {
                    System.out.println("Supprimer: erreur " + response.getStatus());
                }
        }
        else System.out.println("La recherche de la resource a modifier n'a pas abouti !"); 
    }
    
    public void modifier(Achat achat){
        int id = achat.getId().intValue();
        if(trouver(id)){
            Response response = Boutique_web_HTTP_request.getRequest(url, "/update").post(Entity.json(achat));

            if (response.getStatus() == 204) {
                System.out.println("Operation reussi !!");
                System.out.println("Liste des achats: "); 
                this.liste();
            } 
            else {
                    System.out.println("Modifier: erreur " + response.getStatus());
                }
        }
        else System.out.println("La recherche de la resource a modifier n'a pas abouti !"); 
    }
}
