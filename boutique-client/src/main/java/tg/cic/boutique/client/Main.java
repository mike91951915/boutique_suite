/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package tg.cic.boutique.client;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import tg.cic.boutique.entites.Achat;
import tg.cic.boutique.entites.Client;
import tg.cic.boutique.entites.Employe;

/**
 *
 * @author Hisoka
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /** ############...... TEST CODE ......############
         *
         * Affichage de la liste des produits.
         * Ajout d'un nouveau (4eme) client... Client("q65ngzc0bi9u16r", "4120 7312 4095 4301 09/26 613", 4L, "Dalton", "Joe", LocalDate.of(1997, Month.MARCH, 21)).
         * Recherche du produit rechercher par le client (id = 1).
         * Le client effectue des achats; enregistrement de l'achat (4eme)... Achat(4L, LocalDateTime.of(LocalDate.of(2022, Month.SEPTEMBER, 17), LocalTime.of(16, 21, 12)), 0.37).
         * Enregistrement le l'employe qui a effectue la vente (4eme)... Employe("ao53h", 4L, "Porgasse", "Ace", LocalDate.of(2001, Month.JULY, 18)).
         * Erreur lors de lenregistrement de l'employe; modification des donnees de l'employe dont l'id = 4... Employe("ao53h", 4L, "Porgas DI"****, "Ace", LocalDate.of(2001, Month.JULY, 18)).
         * Rupture de stock par rapport au produit paye par le client, suppression du produit concerne (id = 1).
         */
        
        ProduitClient p_client = new ProduitClient();
        ClientClient c_client = new ClientClient();
        AchatClient a_client = new AchatClient();
        EmployeClient e_client = new EmployeClient();
        
        System.out.println("\n\n\n");
        
        System.out.println("############...... Affichage de la liste des produit ......############.");
        p_client.liste();
        
        System.out.print("\n");
        
        System.out.println("############...... Ajout d'un nouveau (4eme) client ......############");
        c_client.ajouter(new Client("q65ngzc0bi9u16r", "4120 7312 4095 4301 09/26 613", 4L, "Dalton", "Joe", LocalDate.of(1997, Month.MARCH, 21)));
        
        System.out.print("\n");
        
        System.out.println("############...... Recherche du produit rechercher par le client (id = 1) ......############");
        p_client.trouver(1);
        
        System.out.print("\n");
        
        System.out.println("############...... Le client effectue des achats; enregistrement de l'achat ......############");
        a_client.ajouter(new Achat(4L, LocalDateTime.of(LocalDate.of(2022, Month.SEPTEMBER, 17), LocalTime.of(16, 21, 12)), 0.37));
        
        System.out.print("\n");
        
        System.out.println("############...... Enregistrement le l'employe qui a effectue la vente ......############");
        e_client.ajouter(new Employe("ao53h", 4L, "Porgasse", "Ace", LocalDate.of(2001, Month.JULY, 18)));
        
        System.out.print("\n");
        
        System.out.println("############...... Erreur lors de l'enregistrement de l'employe; modification des donnees de l'employe dont l'id = 4 ......############");
        e_client.modifier(new Employe("ao53h", 4L, "Porgas DI", "Ace", LocalDate.of(2001, Month.JULY, 18)));
        
        System.out.print("\n");
        
        System.out.println("############...... Rupture de stock par rapport au produit paye par le client, suppression du produit concerne (id = 1) ......############");
        p_client.supprimer(1);
        
        
        
    }
    
}
