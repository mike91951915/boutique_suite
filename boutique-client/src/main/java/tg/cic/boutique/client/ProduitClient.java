/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tg.cic.boutique.client;

import java.util.LinkedList;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import tg.cic.boutique.entites.Produit;

/**
 *
 * @author madara
 */

public class ProduitClient {
    
    private final String url = "http://localhost:8080/boutique-web/resources/produit";
    private Boutique_web_HTTP_request request;

    public ProduitClient() {
    }
    
    public void liste(){
        Response response = Boutique_web_HTTP_request.getRequest(url, "/liste").get();
    
        if (response.getStatus() == 200) {
            LinkedList<Produit> liste_produit = response.readEntity(new GenericType<LinkedList<Produit>>(){});
            System.out.println(liste_produit);
        } 
        else {
                System.out.println("Lister: erreur " + response.getStatus());
            }
    }
    
    public boolean trouver(int id){
        Response response = Boutique_web_HTTP_request.getRequest(url, "/trouver/" + id).get();
    
        if (response.getStatus() == 200) {
            Produit produit = response.readEntity(Produit.class);
            System.out.println(produit);
            return true;
        } 
        else if (response.getStatus() == 204) {
            System.out.println("La resource demande est introuvable"); 
            return false;
        }
        else {
                System.out.println("Trouver: erreur " + response.getStatus());
                return false;
            }
    }
    
    public void compter(){
        Response response = Boutique_web_HTTP_request.getRequest(url, "/nombre").get();
    
        if (response.getStatus() == 200) {
            int nombre_produit = response.readEntity(int.class);
            System.out.println(nombre_produit);
        } 
        else {
                System.out.println("Copmter: erreur " + response.getStatus());
            }
    }
    
    public void ajouter(Produit produit){
        Response response = Boutique_web_HTTP_request.getRequest(url, "").post(Entity.json(produit));
    
        if (response.getStatus() == 204) {
            System.out.println("Operation reussi !!");
            System.out.println("Liste des produits: "); 
            this.liste();
        } 
        else {
                System.out.println("Ajouter: erreur " + response.getStatus());
            }
    }
    
    public void supprimer(int id){
        if(trouver(id)){
            Response response = Boutique_web_HTTP_request.getRequest(url, "/" + id).delete();

            if (response.getStatus() == 204) {
                System.out.println("Operation reussi !!");
                System.out.println("Liste des produits: "); 
                this.liste();
            } 
            else {
                    System.out.println("Supprimer: erreur " + response.getStatus());
                }
        }
        else System.out.println("La recherche de la resource a modifier n'a pas abouti !"); 
    }
    
    public void modifier(Produit produit){
        int id = produit.getId().intValue();
        if(trouver(id)){
            Response response = Boutique_web_HTTP_request.getRequest(url, "/update").post(Entity.json(produit));

            if (response.getStatus() == 204) {
                System.out.println("Operation reussi !!");
                System.out.println("Liste des produits: "); 
                this.liste();
            } 
            else {
                    System.out.println("Modifier: erreur " + response.getStatus());
                }
        }
        else System.out.println("La recherche de la resource a modifier n'a pas abouti !"); 
    }
}
