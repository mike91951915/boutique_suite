/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tg.cic.boutique.client;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;

/**
 *
 * @author Hisoka
 */
public class Boutique_web_HTTP_request {
    
    public static Invocation.Builder getRequest(String url_resource, String chemin) {
        
        Client client = ClientBuilder.newClient();
        Invocation.Builder request = client.target(url_resource)
             .path(chemin)
             .request();
        
        return request;
    }
    
}
