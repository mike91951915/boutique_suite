/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tg.cic.boutique.client;

import java.util.LinkedList;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import tg.cic.boutique.entites.Employe;

/**
 *
 * @author madara
 */

public class EmployeClient {
    
    private final String url = "http://localhost:8080/boutique-web/resources/employe";
    private Boutique_web_HTTP_request request;

    public EmployeClient() {
    }
    
    public void liste(){
        Response response = Boutique_web_HTTP_request.getRequest(url, "/liste").get();
    
        if (response.getStatus() == 200) {
            LinkedList<Employe> liste_employe = response.readEntity(new GenericType<LinkedList<Employe>>(){});
            System.out.println(liste_employe);
        } 
        else {
                System.out.println("Lister: erreur " + response.getStatus());
            }
    }
    
    public boolean trouver(int id){
        Response response = Boutique_web_HTTP_request.getRequest(url, "/trouver/" + id).get();
    
        if (response.getStatus() == 200) {
            Employe employe = response.readEntity(Employe.class);
            System.out.println(employe);
            return true;
        } 
        else if (response.getStatus() == 204) {
            System.out.println("La resource demande est introuvable"); 
            return false;
        }
        else {
                System.out.println("Trouver: erreur " + response.getStatus());
                return false;
            }
    }
    
    public void compter(){
        Response response = Boutique_web_HTTP_request.getRequest(url, "/nombre").get();
    
        if (response.getStatus() == 200) {
            int nombre_employe = response.readEntity(int.class);
            System.out.println(nombre_employe);
        } 
        else {
                System.out.println("Copmter: erreur " + response.getStatus());
            }
    }
    
    public void ajouter(Employe employe){
        Response response = Boutique_web_HTTP_request.getRequest(url, "").post(Entity.json(employe));
    
        if (response.getStatus() == 204) {
            System.out.println("Operation reussi !!");
            System.out.println("Liste des employes: "); 
            this.liste();
        } 
        else {
                System.out.println("Ajouter: erreur " + response.getStatus());
            }
    }
    
    public void supprimer(int id){
        if(trouver(id)){
            Response response = Boutique_web_HTTP_request.getRequest(url, "/" + id).delete();

            if (response.getStatus() == 204) {
                System.out.println("Operation reussi !!");
                System.out.println("Liste des employes: "); 
                this.liste();
            } 
            else {
                    System.out.println("Supprimer: erreur " + response.getStatus());
                }
        }
        else System.out.println("La recherche de la resource a modifier n'a pas abouti !"); 
    }
    
    public void modifier(Employe employe){
        int id = employe.getId().intValue();
        if(trouver(id)){
            Response response = Boutique_web_HTTP_request.getRequest(url, "/update").post(Entity.json(employe));

            if (response.getStatus() == 204) {
                System.out.println("Operation reussi !!");
                System.out.println("Liste des employes: "); 
                this.liste();
            } 
            else {
                    System.out.println("Modifier: erreur " + response.getStatus());
                }
        }
        else System.out.println("La recherche de la resource a modifier n'a pas abouti !"); 
    }
}
