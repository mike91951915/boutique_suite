/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tg.cic.boutique.web.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.util.*;
import tg.cic.boutique.entites.Achat;
/**
 *
 * @author madara
 */
public class AchatService {
    
    private static LinkedList<Achat> ListeAchat = new LinkedList<>();

    public AchatService() {
        
        ListeAchat.add(new Achat(1L, LocalDateTime.of(LocalDate.of(2021, Month.APRIL, 27), LocalTime.of(13, 42, 12)), 0.05));
        ListeAchat.add(new Achat(2L, LocalDateTime.of(LocalDate.of(2022, Month.MARCH, 2), LocalTime.of(20, 12, 19)), 0.1));
        ListeAchat.add(new Achat(3L, LocalDateTime.of(LocalDate.of(2021, Month.SEPTEMBER, 11), LocalTime.of(9, 51, 0)), 0.02));
    }

    public static LinkedList<Achat> getListeAchat() {
        return ListeAchat;
    }

    public static void setListeAchat(LinkedList<Achat> ListeAchat) {
        AchatService.ListeAchat = ListeAchat;
    }

    public void ajouter(Achat e){
        //ListeAchat.add(e);
    }
    
    public void modifier(Achat e){
        
        for(Achat element : ListeAchat){
            if(Objects.equals(element.getId(), e.getId())){
                int index = ListeAchat.indexOf(element);
                ListeAchat.remove(element);
                ListeAchat.add(index, e);
                break;
            }
        }
    }
    
    public void supprimer(long id){
        for(Achat element : ListeAchat){
            if(element.getId() == id){
                ListeAchat.remove(element);
                break;
            }
        }
    }
    
    public LinkedList<Achat> lister(){
        return AchatService.getListeAchat();
    }
    
    public Achat trouver(long id){
        for(Achat element : ListeAchat){
            if(element.getId() == id){
                return element;
            }
        }
        return null;
    }
    
    public int compter(){
        return ListeAchat.size();
    }
}
