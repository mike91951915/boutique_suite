/*
 * Click nbfs://nbhost/SystemFileSystem/Achatemplates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Achatemplates/Classes/Class.java to edit this template
 */
package tg.cic.boutique.web.resources;

import java.util.*;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import tg.cic.boutique.entites.Achat;
import tg.cic.boutique.web.service.AchatService;
/**
 *
 * @author madara
 */

@Path("/achat")
public class AchatResource {
    
    
    private static AchatService service;
    
    public AchatResource() {
        if(AchatResource.service == null ) AchatResource.service = new AchatService();
    }
    
    @GET
    @Path("/liste")
    //@Produces(MediaType.APPLICATION_XML)
    public LinkedList<Achat> lister()
    {
       return service.lister();
    }
    
    @GET
    @Path("/trouver/{id}")
    //@Produces(MediaType.APPLICATION_XML)
    public Achat trouver(@PathParam("id") Long id){
        return service.trouver(id);
    }
    
    @GET
    @Path("/nombre")
    public int compter(){
        return service.compter();
    }
    
    @POST
    public void ajouter(Achat achat){
        service.ajouter(achat);
    }
    
    @POST
    @Path("/update")
    public void modifier(Achat achat){
        service.modifier(achat);
    }
    
    @DELETE
    @Path("/{id}")
    public void supprimer(@PathParam("id") Long id){
        service.supprimer(id);
    }
    
}
