/*
 * Click nbfs://nbhost/SystemFileSystem/Categorieemplates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Categorieemplates/Classes/Class.java to edit this template
 */
package tg.cic.boutique.web.resources;

import java.util.*;
import javax.ws.rs.*;
import tg.cic.boutique.entites.Categorie;
import tg.cic.boutique.web.service.CategorieService;
/**
 *
 * @author madara
 */

@Path("/categorie")
public class CategorieResource {
    
    
    private static CategorieService service;
    
    public CategorieResource() {
        if(this.service == null ) this.service = new CategorieService();
    }
    
    @GET
    @Path("/liste")
    public LinkedList<Categorie> lister()
    {
       return service.lister();
    }
    
    @GET
    @Path("/trouver/{id}")
    public Categorie trouver(@PathParam("id") Long id){
        return service.trouver(id);
    }
    
    @GET
    @Path("/nombre")
    public int compter(){
        return service.compter();
    }
    
    @POST
    public void ajouter(Categorie categorie){
        service.ajouter(categorie);
    }
    
    @POST
    @Path("/update")
    public void modifier(Categorie categorie){
        service.moidifier(categorie);
    }
    
    @DELETE
    @Path("/{id}")
    public void supprimer(@PathParam("id") Long id){
        service.supprimer(id);
    }
    
}
