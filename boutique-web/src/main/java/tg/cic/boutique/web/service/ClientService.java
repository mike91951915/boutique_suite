/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tg.cic.boutique.web.service;

import java.time.LocalDate;
import java.time.Month;
import java.util.*;
import tg.cic.boutique.entites.Client;
/**
 *
 * @author madara
 */
public class ClientService {
    
    private static LinkedList<Client> ListeClient;

    public ClientService() {
        ListeClient = new LinkedList<>();
        
        ListeClient.add(new Client("fgn848nmg3hmmj5l", "4970 1012 3456 7890 08/23 542", 1L, "Pain", "Tendo", LocalDate.of(1987, Month.MARCH, 11)));
        ListeClient.add(new Client("wr6etr5ty61i5316", "4871 9400 7047 5980 07/23 752", 2L, "Uzumaki", "Naruto", LocalDate.of(2005, Month.OCTOBER, 10)));
        ListeClient.add(new Client("z6c86vvb51656fg1", "5412 7512 3456 7890 12/16 683", 3L, "Tepes", "Vladmir", LocalDate.of(0000, Month.JANUARY, 1)));
    }

    public static LinkedList<Client> getListeClient() {
        return ListeClient;
    }

    public static void setListeClient(LinkedList<Client> ListeClient) {
        ClientService.ListeClient = ListeClient;
    }

    public void ajouter(Client e){
        ListeClient.add(e);
    }
    
    public void moidifier(Client e){
        for(Client element : ListeClient){
            if(Objects.equals(element.getId(), e.getId())){
                int index = ListeClient.indexOf(element);
                ListeClient.remove(element);
                ListeClient.add(index, e);
                break;
            }
        }
    }
    
    public void supprimer(long id){
        for(Client element : ListeClient){
            if(element.getId() == id){
                ListeClient.remove(element);
                break;
            }
        }
    }
    
    public LinkedList<Client> lister(){
        return ClientService.getListeClient();
    }
    
    public Client trouver(long id){
        for(Client element : ListeClient){
            if(element.getId() == id){
                return element;
            }
        }
        return null;
    }
    
    public int compter(){
        return ListeClient.size();
    }
}
