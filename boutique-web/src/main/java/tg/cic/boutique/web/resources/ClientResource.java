/*
 * Click nbfs://nbhost/SystemFileSystem/Clientemplates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Clientemplates/Classes/Class.java to edit this template
 */
package tg.cic.boutique.web.resources;

import java.util.*;
import javax.ws.rs.*;
import tg.cic.boutique.entites.Client;
import tg.cic.boutique.web.service.ClientService;
/**
 *
 * @author madara
 */

@Path("/client")
public class ClientResource {
    
    
    private static ClientService service;
    
    public ClientResource() {
        if(this.service == null ) this.service = new ClientService();
    }
    
    @GET
    @Path("/liste")
    public LinkedList<Client> lister()
    {
       return service.lister();
    }
    
    @GET
    @Path("/trouver/{id}")
    public Client trouver(@PathParam("id") Long id){
        return service.trouver(id);
    }
    
    @GET
    @Path("/nombre")
    public int compter(){
        return service.compter();
    }
    
    @POST
    public void ajouter(Client client){
        service.ajouter(client);
    }
    
    @POST
    @Path("/update")
    public void modifier(Client client){
        service.moidifier(client);
    }
    
    @DELETE
    @Path("/{id}")
    public void supprimer(@PathParam("id") Long id){
        service.supprimer(id);
    }
    
}
