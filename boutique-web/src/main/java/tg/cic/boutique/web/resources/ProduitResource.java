/*
 * Click nbfs://nbhost/SystemFileSystem/Produitemplates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Produitemplates/Classes/Class.java to edit this template
 */
package tg.cic.boutique.web.resources;

import java.util.*;
import javax.ws.rs.*;
import tg.cic.boutique.entites.Produit;
import tg.cic.boutique.web.service.ProduitService;
/**
 *
 * @author madara
 */

@Path("/produit")
public class ProduitResource {

    private static ProduitService service;
    
    public ProduitResource() {
        if(this.service == null ) this.service = new ProduitService();
    }
    
    @GET
    @Path("/liste")
    public LinkedList<Produit> lister()
    {
       return service.lister();
    }
    
    @GET
    @Path("/trouver/{id}")
    public Produit trouver(@PathParam("id") Long id){
        return service.trouver(id);
    }
    
    @GET
    @Path("/nombre")
    public int compter(){
        return service.compter();
    }
    
    @POST
    public void ajouter(Produit produit){
        service.ajouter(produit);
    }
    
    @POST
    @Path("/update")
    public void modifier(Produit produit){
        service.moidifier(produit);
    }
    
    @DELETE
    @Path("/{id}")
    public void supprimer(@PathParam("id") Long id){
        service.supprimer(id);
    }
    
}
