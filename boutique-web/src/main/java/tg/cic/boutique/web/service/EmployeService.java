/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tg.cic.boutique.web.service;


import java.time.LocalDate;
import java.time.Month;
import java.util.*;
import tg.cic.boutique.entites.Employe;
/**
 *
 * @author madara
 */
public class EmployeService {
    
    private static LinkedList<Employe> ListeEmploye;

    public static LinkedList<Employe> getListeEmploye() {
        return ListeEmploye;
    }

    public EmployeService() {
        ListeEmploye = new LinkedList<>();
        
        ListeEmploye.add(new Employe("bdh31", 1L, "Yagami", "Light", LocalDate.of(1999, Month.FEBRUARY, 28)));
        ListeEmploye.add(new Employe("ium361", 2L, "Vi Britania", "Lelouch", LocalDate.of(2004, Month.DECEMBER, 5)));
        ListeEmploye.add(new Employe("48yjs", 3L, "Fan", "Zhuo", LocalDate.of(2004, Month.JUNE, 28)));
    }
    
    public static void setListeEmploye(LinkedList<Employe> ListeEmploye) {
        EmployeService.ListeEmploye = ListeEmploye;
    }

    public void ajouter(Employe e){
        ListeEmploye.add(e);
    }
    
    public void moidifier(Employe e){
        for(Employe element : ListeEmploye){
            if(Objects.equals(element.getId(), e.getId())){
                int index = ListeEmploye.indexOf(element);
                ListeEmploye.remove(element);
                ListeEmploye.add(index, e);
                break;
            }
        }
    }
    
    public void supprimer(long id){
        for(Employe element : ListeEmploye){
            if(element.getId() == id){
                ListeEmploye.remove(element);
                break;
            }
        }
    }
    
    public LinkedList<Employe> lister(){
        return EmployeService.getListeEmploye();
    }
    
    public Employe trouver(long id){
        for(Employe element : ListeEmploye){
            if(element.getId() == id){
                return element;
            }
        }
        return null;
    }
    
    public int compter(){
        return ListeEmploye.size();
    }
}
