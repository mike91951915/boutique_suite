/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tg.cic.boutique.web.resources;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.*;

/**
 *
 * @author madara
 */
@Provider
public class MyExceptionMapper implements ExceptionMapper<Exception> {
    
    @Override
    public Response toResponse(Exception e) {
        e.printStackTrace();
        return Response
                        .status(Status.INTERNAL_SERVER_ERROR)
                        .entity(e.getCause())
                        .build();
    }
    
}
