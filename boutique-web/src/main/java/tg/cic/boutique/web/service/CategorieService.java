/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tg.cic.boutique.web.service;

import java.util.*;
import tg.cic.boutique.entites.Categorie;
/**
 *
 * @author madara
 */
public class CategorieService {
    
    private static LinkedList<Categorie> ListeCategorie;

    public CategorieService() {
        ListeCategorie = new LinkedList<>();
        
        ListeCategorie.add(new Categorie(1L, "Chaussure", "Luis Vuitton, Paris"));
        ListeCategorie.add(new Categorie(2L, "Voiture", "Koeniggseg, 500+ mph"));
        ListeCategorie.add(new Categorie(3L, "Organe", "Organe vitaux, les plus essentiels"));
    }

    public static LinkedList<Categorie> getListeCategorie() {
        return ListeCategorie;
    }

    public static void setListeCategorie(LinkedList<Categorie> ListeCategorie) {
        CategorieService.ListeCategorie = ListeCategorie;
    }

    public void ajouter(Categorie e){
        ListeCategorie.add(e);
    }
    
    public void moidifier(Categorie e){
        for(Categorie element : ListeCategorie){
            if(Objects.equals(element.getId(), e.getId())){
                int index = ListeCategorie.indexOf(element);
                ListeCategorie.remove(element);
                ListeCategorie.add(index, e);
                break;
            }
        }
    }
    
    public void supprimer(long id){
        for(Categorie element : ListeCategorie){
            if(element.getId() == id){
                ListeCategorie.remove(element);
                break;
            }
        }
    }
    
    public LinkedList<Categorie> lister(){
        return CategorieService.getListeCategorie();
    }
    
    public Categorie trouver(long id){
        for(Categorie element : ListeCategorie){
            if(element.getId() == id){
                return element;
            }
        }
        return null;
    }
    
    public int compter(){
        return ListeCategorie.size();
    }
}
