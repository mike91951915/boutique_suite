/*
 * Click nbfs://nbhost/SystemFileSystem/Employeemplates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Employeemplates/Classes/Class.java to edit this template
 */
package tg.cic.boutique.web.resources;

import java.util.*;
import javax.ws.rs.*;
import tg.cic.boutique.entites.Employe;
import tg.cic.boutique.web.service.EmployeService;
/**
 *
 * @author madara
 */

@Path("/employe")
public class EmployeResource {
    
    
    private static EmployeService service;
    
    public EmployeResource() {
        if(this.service == null ) this.service = new EmployeService();
    }
    
    @GET
    @Path("/liste")
    public LinkedList<Employe> lister()
    {
       return service.lister();
    }
    
    @GET
    @Path("/trouver/{id}")
    public Employe trouver(@PathParam("id") Long id){
        return service.trouver(id);
    }
    
    @GET
    @Path("/nombre")
    public int compter(){
        return service.compter();
    }
    
    @POST
    public void ajouter(Employe employe){
        service.ajouter(employe);
    }
    
    @POST
    @Path("/update")
    public void modifier(Employe employe){
        service.moidifier(employe);
    }
    
    @DELETE
    @Path("/{id}")
    public void supprimer(@PathParam("id") Long id){
        service.supprimer(id);
    }
    
}
