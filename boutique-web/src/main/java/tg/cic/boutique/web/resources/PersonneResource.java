/*
 * Click nbfs://nbhost/SystemFileSystem/Personneemplates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Personneemplates/Classes/Class.java to edit this template
 */
package tg.cic.boutique.web.resources;

import java.util.*;
import javax.ws.rs.*;
import tg.cic.boutique.entites.Personne;
import tg.cic.boutique.web.service.PersonneService;

/**
 *
 * @author madara
 */

@Path("/personne")
public class PersonneResource{
    
    
    private static PersonneService service;
    
    public PersonneResource() {
        if(this.service == null ) this.service = new PersonneService();
    }
    
    @GET
    @Path("/liste")
    public LinkedList<Personne> lister()
    {
       return service.lister();
    }
    
    @GET
    @Path("/trouver/{id}")
    public Personne trouver(@PathParam("id") Long id){
        return service.trouver(id);
    }
    
    @GET
    @Path("/nombre")
    public int compter(){
        return service.compter();
    }
    
    @POST
    public void ajouter(Personne personne){
        service.ajouter(personne);
    }
    
    @POST
    @Path("/update")
    public void modifier(Personne personne){
        service.moidifier(personne);
    }
    
    @DELETE
    @Path("/{id}")
    public void supprimer(@PathParam("id") Long id){
        service.supprimer(id);
    }
    
}
