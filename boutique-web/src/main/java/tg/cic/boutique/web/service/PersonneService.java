/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tg.cic.boutique.web.service;

import java.time.LocalDate;
import java.time.Month;
import java.util.*;
import tg.cic.boutique.entites.Personne;
/**
 *
 * @author madara
 */
public class PersonneService {
    
    private static LinkedList<Personne> ListePersonne;

    public PersonneService() {
        PersonneService.ListePersonne = new LinkedList<>();
        
        ListePersonne.add(new Personne(1L, "Uchiha", "Madara", LocalDate.of(1986, Month.DECEMBER, 24)));
        ListePersonne.add(new Personne(2L, "Sosuke", "Aisen", LocalDate.of(1912, Month.MAY, 29)));
        ListePersonne.add(new Personne(3L, "Zi Britania", "Charles", LocalDate.of(1959, Month.OCTOBER, 15)));
    }
    
    public static LinkedList<Personne> getListePersonne() {
        return ListePersonne;
    }

    public static void setListePersonne(LinkedList<Personne> ListePersonne) {
        PersonneService.ListePersonne = ListePersonne;
    }

    public void ajouter(Personne e){
        ListePersonne.add(e);
    }
    
    public void moidifier(Personne e){
        for(Personne element : ListePersonne){
            if(Objects.equals(element.getId(), e.getId())){
                int index = ListePersonne.indexOf(element);
                ListePersonne.remove(element);
                ListePersonne.add(index, e);
                break;
            }
        }
    }
    
    public void supprimer(long id){
        for(Personne element : ListePersonne){
            if(element.getId() == id){
                ListePersonne.remove(element);
                break;
            }
        }
    }
    
    public LinkedList<Personne> lister(){
        return PersonneService.getListePersonne();
    }
    
    public Personne trouver(long id){
        for(Personne element : ListePersonne){
            if(element.getId() == id){
                return element;
            }
        }
        return null;
    }
    
    public int compter(){
        return ListePersonne.size();
    }
}
