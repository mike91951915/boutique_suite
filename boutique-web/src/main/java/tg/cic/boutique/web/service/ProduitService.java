/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tg.cic.boutique.web.service;

import tg.cic.boutique.entites.*;
import java.time.LocalDate;
import java.util.*;
/**
 *
 * @author madara
 */
public class ProduitService {
    
    private static LinkedList<Produit> ListeProduit;

    public static LinkedList<Produit> getListeProduit() {
        return ListeProduit;
    }

    public static void setListeProduit(LinkedList<Produit> ListeProduit) {
        ProduitService.ListeProduit = ListeProduit;
    }
    
    public ProduitService() {
        ProduitService.ListeProduit = new LinkedList<>();
        Categorie c1 = new Categorie(1L, "Chaussure", "Luis Vuitton, Paris");
        Categorie c2 = new Categorie(2L, "Voiture", "Koeniggseg, 500+ mph");
        ListeProduit.add(new Produit(1L, "Koeniggseg Agera RS", 4100000.0, LocalDate.of(2022, 9, 11)));
        ListeProduit.add(new Produit(2L, "Luis basket 1", 410.0, LocalDate.of(2023, 9, 11)));
        ListeProduit.add(new Produit(3L, "Koeniggseg Regera", 5360000.0, LocalDate.of(2024, 9, 11)));
    }

    public void ajouter(Produit e){
        ListeProduit.add(e);
    }
    
    public void moidifier(Produit e){
        for(Produit element : ListeProduit){
            if(Objects.equals(element.getId(), e.getId())){
                int index = ListeProduit.indexOf(element);
                ListeProduit.remove(element);
                ListeProduit.add(index, e);
                break;
            }
        }
    }
    
    public void supprimer(long id){
        ListeProduit.remove(this.trouver(id));
    }
    
    public LinkedList<Produit> lister(){
        return ProduitService.getListeProduit();
    }
    
    public Produit trouver(long id){
        return ListeProduit.get((int)id - 1);
    }
    
    public int compter(){
        return ListeProduit.size();
    }
}
